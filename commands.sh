#!/bin/bash
mkdir -p ~/projects/stam/LO3/unit-test
code ~/projects/stam/LO3/unit-test

#terminal
git init
npm init -y

npm install --save express
npm install --save-dev mocha chai
echo "node_modules" > .gitignore



touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

node src/main.js
node --watch-path=src src/main.js

#test
npm run test

# GIT
git remote add origin https://gitlab.com/{Nikotakala}/unit-test
$ git add . --dry-ryn
git add .
git status
git commit -m "initial commit"

git push -u origin main