// src/calc.js

/**
* Adds two numbers together
* @param {number} a
* @param {number} b
* @returns {number}
*/
const add = (a, b) => a + b;
/**
* Subtracts number from minuend
* @param {number} minuend
* @param {number} subtrahend
* @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
}
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * divides to numbers.
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number} 
 * @throws {Error} 0 division
 */

const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
    const fraction = dividend / divisor;
    return fraction;
}

export default { add, subtract, divide, multiply }